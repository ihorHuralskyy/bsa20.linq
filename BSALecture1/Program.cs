﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using BSALecture1.LB.Models;
using BSALecture1.LB.ResultModels;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace BSALecture1
{
    class Program
    {
        public static async Task<List<BSALecture1.LB.Models.Task>> GetAllTasks()
        {
            return await _client.GetAsync("https://bsa20.azurewebsites.net/api/Tasks").Result.Content.ReadAsAsync<List<BSALecture1.LB.Models.Task>>();
        }

        public static async Task<List<Project>> GetAllProjects()
        {
            return await _client.GetAsync("https://bsa20.azurewebsites.net/api/Projects").Result.Content.ReadAsAsync<List<Project>>();
        }

        public static async Task<List<Team>> GetAllTeams()
        {
            return await _client.GetAsync("https://bsa20.azurewebsites.net/api/Teams").Result.Content.ReadAsAsync<List<Team>>();
        }

        public static async Task<List<User>> GetAllUsers()
        {
            return await _client.GetAsync("https://bsa20.azurewebsites.net/api/Users").Result.Content.ReadAsAsync<List<User>>();
        }

        public static async Task<User> GetUser(int id)
        {
            return await _client.GetAsync("https://bsa20.azurewebsites.net/api/Users/id").Result.Content.ReadAsAsync<User>();
        }

        public static async Task<Dictionary<Project,int>> GetAmmountTasksOfProjectbyUserId(int id)
        {
            var tasks = await GetAllTasks();
            return (await GetAllProjects()).Where(y => y.AuthorId == id)
                .Select(x => new { project = x, numOfTasks = tasks.Where(z => z.ProjectId == x.Id).Count() })
                .ToDictionary(x => x.project, x => x.numOfTasks);
        }

        public static async Task<List<BSALecture1.LB.Models.Task>> GetTasksShorter45ByUserId(int id)
        {
            return (await GetAllTasks()).Where(y => y.PerformerId == id && y.Name.Length < 45).ToList();
        }

        public static async Task<List<TaskNameId>> GetTasksFinishedIn2020ByUserId(int id)
        {
            return (await GetAllTasks()).Where(y => y.PerformerId == id && y.State == TaskState.Finished && y.FinishedAt.Year == 2020)
                .Select(x => new TaskNameId { Id = x.Id, Name = x.Name })
                .ToList();
        }

        public static async Task<List<TeamUsers>> GetTeamsWithUsersOlderThan10()
        {
            return (await GetAllTeams()).GroupJoin((await GetAllUsers()), 
                t => t.Id, 
                u => u.TeamId, 
                (t, u) => new TeamUsers { Id = t.Id,
                    Name = t.Name, 
                    Users = u.OrderByDescending(x => x.RegisteredAt).ToList() })
                .Where(x => x.Users.Count > 0 &&
                x.Users.Count == x.Users.Where(x => x.Birthday.Year <= DateTime.Now.Year - 10).Count())
                .ToList(); 
        }


        public static async Task<List<UserWithTasks>> GetSortedUsersWithSortedTasks()
        {
            return (await GetAllUsers()).GroupJoin((await GetAllTasks()),
                u => u.Id,
                t => t.PerformerId,
                (u, t) => new UserWithTasks
                {
                    User=u,
                    Tasks = t.OrderByDescending(x => x.Name.Length).ToList()
                })
                .OrderBy(x=>x.User.FirstName)
                .ToList();
        }

        public static async Task<UserTasksInfo> GetUserByIdWithTaskInfo(int id)
        {
            var projects = await GetAllProjects();
            var tasks = await GetAllTasks();
            return (await GetAllUsers()).Where(x => x.Id == id)
                .Select(x => new UserTasksInfo
                {
                    User = x,
                    LastProject = projects.Where(y => y.AuthorId == id).OrderByDescending(y => y.CreatedAt).First(),
                    NumberOfTasksInLastProj = tasks.Where(z => z.ProjectId == projects.Where(z => z.AuthorId == id).OrderByDescending(z => z.CreatedAt).First().Id).Count(),
                    NumberOfUnFinishedOrCancelledTasks = tasks.Where(x => x.PerformerId == id && x.State != TaskState.Finished).Count(),
                    LongestTask = tasks.Where(x => x.PerformerId == id).OrderByDescending(x => x.FinishedAt - x.CreatedAt).First()

                }).First();
        }

        public static async Task<List<ProjectInfo>> GetProjectsAdditionalInfo()
        {
            var tasks = await GetAllTasks();         
            var users = await GetAllUsers();
            return (await GetAllProjects()).Where(x=>x.Description.Length>20 || tasks.Where(y => y.ProjectId == x.Id).Count()<3)
                .Select(x => new ProjectInfo
                {
                    Project = x,
                    LongestDescriptionTask = tasks.Where(y=>y.ProjectId==x.Id).OrderByDescending(x=>x.Description.Length).First(),
                    ShortestNameTask = tasks.Where(y=>y.ProjectId==x.Id).OrderBy(x=>x.Name.Length).First(),
                    NumberOfUsers = users.Where(y=>y.TeamId==x.TeamId).Count()
                }).ToList();
        }

        private static HttpClient _client;
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            _client = new HttpClient();
            bool endApp = false;
            while (!endApp)
            {
                Console.WriteLine("\n\n\tMenu:\n");
                Console.WriteLine("1.Press 1 to Get the number of tasks in the project of a particular user (by id) (dictionary, key will be the project, the value of the number of tasks)..\n");
                Console.WriteLine("2.Press 2 to Get a list of tasks designed for a specific user (by id), where name task <45 characters (collection of tasks)..\n");
                Console.WriteLine("3.Press 3 to Get a list (id, name) from the collection of tasks that are completed in the current (2020) year for a specific user (by id).\n");
                Console.WriteLine("4.Press 4 to Get a list (id, team name and user list) from a collection of teams over 10 years old, sorted by date of user registration in descending order, and grouped by team.\n");
                Console.WriteLine("5.Press 5 to Get a list of users in alphabetical order first_name (ascending) with sorted tasks by length name (descending)..\n");
                Console.WriteLine("6.Press 6 to Get the following structure (pass user id to parameters):User,Last user project(by creation date),The total number of tasks under the last project,The total number of incomplete or canceled tasks for the user,The user's longest task by date (earliest created - latest completed).\n");
                Console.WriteLine("7.Press 7 to Get the following structure: Project, The longest task of the project(according to the description), The shortest task of the project(by name), The total number of users in the project team, where either the project description > 20 characters, or the number of tasks < 3.\n");
                Console.WriteLine("Press 'e' and Enter to close the app.");

                switch (Console.ReadLine())
                {
                    case "1":
                        try
                        {
                            Console.WriteLine("Input user's id:");
                            string idInput = Console.ReadLine();
                            int.TryParse(idInput, out int id);
                            var userProjects = await GetAmmountTasksOfProjectbyUserId(id);
                            Console.WriteLine("Projects:");
                            foreach (var item in userProjects)
                            {
                                Console.WriteLine($"AuthorId = {item.Key.AuthorId} ProjectId = {item.Key.Id}. Ammount of tasks: {item.Value}");
                            }

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "2":
                        try
                        {
                            Console.WriteLine("Input user's id:");
                            string idInput = Console.ReadLine();
                            int.TryParse(idInput, out int id);
                            var userTasks = await GetTasksShorter45ByUserId(id);
                            foreach (var item in userTasks)
                            {
                                Console.WriteLine($"UserId = {item.PerformerId}. Name of the task: {item.Name}");
                            }

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "3":
                        try
                        {
                            Console.WriteLine("Input user's id:");
                            string idInput = Console.ReadLine();
                            int.TryParse(idInput, out int id);
                            var finishedTasks = await GetTasksFinishedIn2020ByUserId(id);
                            foreach (var item in finishedTasks)
                            {
                                Console.WriteLine($"UserId = {item.Id}. Name of the task: {item.Name}");
                            }

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "4":
                        try
                        {
                            var teamOld = await GetTeamsWithUsersOlderThan10();
                            foreach (var item in teamOld)
                            {
                                Console.WriteLine($"TeamId = {item.Id}. Name of the team: {item.Name}\nUsers:");
                                foreach(var el in item.Users)
                                {
                                    Console.WriteLine($"\tUserId = {el.Id}, birthday = {el.Birthday}, registeredAt = {el.RegisteredAt}, teamId = {el.TeamId}");
                                }
                            }

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;

                    case "5":
                        try
                        {
                            var usersTasks = await GetSortedUsersWithSortedTasks();

                            foreach (var item in usersTasks)
                            {
                                Console.WriteLine($"UserId = {item.User.Id}. First name: {item.User.FirstName}\nTasks:");
                                foreach (var el in item.Tasks)
                                {
                                    Console.WriteLine($"\tperformerId = {el.PerformerId}, taskId = {el.Id}, task's name = {el.Name}, length of name = {el.Name.Length}");
                                }
                                Console.WriteLine();
                            }

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;

                    case "6":
                        try
                        {
                            Console.WriteLine("Input user's id:");
                            string idInput = Console.ReadLine();
                            int.TryParse(idInput, out int id);
                            var usersTasksInfo = await GetUserByIdWithTaskInfo(id);
                            Console.WriteLine($"UserId = {usersTasksInfo.User.Id}. First name: {usersTasksInfo.User.FirstName}");
                            Console.WriteLine($"Last project: id = {usersTasksInfo.LastProject.Id}, name ={usersTasksInfo.LastProject.Name}, created at = {usersTasksInfo.LastProject.CreatedAt}");
                            Console.WriteLine($"Last project number of tasks : {usersTasksInfo.NumberOfTasksInLastProj}");
                            Console.WriteLine($"Number of unfinished tasks in all user's projects : {usersTasksInfo.NumberOfUnFinishedOrCancelledTasks}");
                            Console.WriteLine($"Longest task : taskId = {usersTasksInfo.LongestTask.Id}, task's name = {usersTasksInfo.LongestTask.Name}, created at = {usersTasksInfo.LongestTask.CreatedAt}, finished at = {usersTasksInfo.LongestTask.FinishedAt}");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;

                    case "7":
                        try
                        {
                            var projectInfo = await GetProjectsAdditionalInfo();
                            Console.WriteLine("List of projects with description>20 or task<3:");
                            foreach(var item in projectInfo)
                            {
                                Console.WriteLine($"Project: id = {item.Project.Id}, name ={item.Project.Name}, description = {item.Project.Description}");
                                Console.WriteLine($"Longest task by description : taskId = {item.LongestDescriptionTask.Id}, description = {item.LongestDescriptionTask.Description}");
                                Console.WriteLine($"Shortest task by name : taskId = {item.ShortestNameTask.Id}, name = {item.ShortestNameTask.Name}");
                                Console.WriteLine($"Total number of users in project : {item.NumberOfUsers}");
                                Console.WriteLine();
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;

                    case "e":
                        endApp = true;
                        break;

                    default:
                        Console.Write("This is not valid input. Please enter value in range [1;10].\n\n ");
                        break;
                }

            }
        }
    }
}
