﻿using BSALecture1.LB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSALecture1.LB.ResultModels
{
    public class UserTasksInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int NumberOfTasksInLastProj { get; set; }
        public int NumberOfUnFinishedOrCancelledTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}
