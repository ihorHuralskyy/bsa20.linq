﻿using BSALecture1.LB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSALecture1.LB.ResultModels
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public Task LongestDescriptionTask { get; set; }
        public Task ShortestNameTask { get; set; }
        public int NumberOfUsers { get; set; }
    }
}
