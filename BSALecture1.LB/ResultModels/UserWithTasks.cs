﻿using BSALecture1.LB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSALecture1.LB.ResultModels
{
    public class UserWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
