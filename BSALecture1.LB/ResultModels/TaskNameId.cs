﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSALecture1.LB.ResultModels
{
    public class TaskNameId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
