﻿using BSALecture1.LB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSALecture1.LB.ResultModels
{
    public class TeamUsers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
