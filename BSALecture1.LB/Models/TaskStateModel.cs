﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSALecture1.LB.Models
{
    public class TaskStateModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
