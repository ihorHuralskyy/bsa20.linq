﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSALecture1.LB.Models
{
    public enum TaskState 
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
